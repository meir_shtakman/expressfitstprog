import express, { NextFunction ,RequestHandler } from 'express';
import log from '@ajar/marker';
import morgan from 'morgan';
import greeting from './greetingRoute.js'

const { PORT, HOST } = process.env;

const app = express()
app.use(express.json())
app.use(morgan('dev'))
app.use((req, res,next) => {
    console.log(req.path);
    next();
})

app.use('/greeting',greeting);

const firstLog:RequestHandler = (req,res,next)=>{
    console.log("first");
    next();
}

const secondLog: RequestHandler = (req,res,next)=>{
    console.log("second");
    next();
}

app.get('/',firstLog,secondLog,  (req, res) => {
    res.status(200).send('Hello Express!')
})

app.get('/users', (req, res) => {
    res.status(200).send('Get all Users')
})

app.get('/search', (req, res) => {
    const js = {food : req.query.food, town: req.query.town};
    res.status(200).json(js);
})

app.get('/param/:any', (req, res) => {
    res.status(200).send(req.params.any)
})

app.post('/body', (req, res) => {
    res.status(200).json(req.body)
})

app.get('/html', (req, res) => {
    res.status(200).set("content-type","text/html").send(`<body>
    <h1>Same Same but Different</h1>
    <p>Export a function called <span>shallow_compare</span> which accept two object parameters - 
      <span>objA , objB</span> as input</p>
    <p> The function will return a boolean as its output<br>
      true if the objects are identical and false if they are not<br>
      **The comparison is 'shallow' which means it checks just one level and no nested properties...</p>
    For example:
    <pre><code>let personA = {
       name:"Tami",
       email: "tami@gmail.com",
       age: 32
    }
    let personB = {
       name:"Tami",
       email: "tami@gmail.com",
       age: 32
    }
    let personC = {
       name:"Tami",
       email: "tami@gmail.com",
       age: 33
    }
    let personD = {
       name:"Tami",
       email: "tami@gmail.com",
       age: 32,
       fav_color : 'orange'
    }
    console.log( shallow_compare( personA , personB ) )</code> // &rarr; true <br>
    <code>console.log( shallow_compare( personB , personC ) )</code> // &rarr; false <br>
    <code>console.log( shallow_compare( personC , personD ) )</code> // &rarr; false <br>
    </pre>
  </p>`)
})

app.use("*",(req, res) => {
    res.status(404).send("Error 404 not found route")
})
// '/search?food=burger&town=ashdod'


app.listen(Number(PORT), String(HOST),  ()=> {
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});

//------------------------------------------
//         Express Echo Server
//------------------------------------------
/* challenge instructions

     - install another middleware - morgan
        configuring app middleware like so:
        app.use( morgan('dev') );

    -  define more routing functions that use

        - req.query - access the querystring part of the request url
        - req.params - access dynamic parts of the url
        - req.body - access the request body of a POST request
        
        in each routing function you want to pass some values to the server from the client
        and echo those back in the server response

    - return api json response
    - return html markup response

    - return 404 status with a custom response to unsupported routes


*/
