import { Router,RequestHandler } from "express";   

const route = Router();
const logRelative:RequestHandler = (req,res,next)=>{
    console.log("greeting - ", req.path);
    next();
}
route.use(logRelative);
route.get('/',  (req, res,next) => {
    res.status(200).send('Greeting from Express!');
})

route.get('/users', (req, res) => {
    res.status(200).send('Greeting to all Users')
})

export default route;